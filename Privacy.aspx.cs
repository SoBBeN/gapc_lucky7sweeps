﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Privacy : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlDataReader dr = Functions.GetWebSite();

        if (dr != null && dr.HasRows)
        {
            if (dr.Read())
            {
                litPrivacy.Text = Convert.ToString(dr["Privacy"]);
            }
            dr.Close();
            dr.Dispose();
        }
    }
}