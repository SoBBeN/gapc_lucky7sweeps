﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SweepContests.aspx.cs" Inherits="SweepContests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({ publisher: "98c30075-93f7-4baa-9669-a5a1792d12a3", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
    <link href="/css/SweepContests.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div id="topcoupon">
        <div class="title"><asp:Literal runat="server" ID="litTitle" /></div>
        <div class="date"><asp:Literal runat="server" ID="litDate" /></div>
        <div class="tags">Categories: <asp:Literal runat="server" ID="litTags" /></div>
        <div class="image">
            <img runat="server" id="imgTopCoupon" />
        </div>
        <div class="text"><asp:Literal runat="server" ID="litText" /></div>
        <div class="more_info">
            <a href="" runat="server" id="lnkBtn" target="_blank">Enter</a>
        </div>
    </div>
    <br />
    <div id="poPrevNext">
        <div class="prev image"><a href="javascript:void(0);" runat="server" id="imgLnkPrev"><img runat="server" id="imgPrev" /></a></div>
        <div class="prev lnk"><a href="javascript:void(0);" runat="server" id="lnkPrev">< Previous</a></div>
        <div class="next image"><a href="javascript:void(0);" runat="server" id="imgLnkNext"><img runat="server" id="imgNext" /></a></div>
        <div class="next lnk"><a href="javascript:void(0);" runat="server" id="lnkNext">Next ></a></div>
        <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
    <br /><br />
</asp:Content>
