﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SweepContestsList : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 5;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["post"] == "yes")
        {
            Response.Write("<script>alert(\"You're information has been submitted.\")</script>");
        }

        SqlDataReader dr;

        dr = Functions.GetSweepContestsList(-1);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                int start = 0;
                if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                {
                    lnkPrev.HRef = "/Contests/?s=" + (start - NB_PER_PAGE).ToString();
                    lnkPrev.Visible = true;
                }

                lnkNext.HRef = "/Contests/?s=" + (start + NB_PER_PAGE).ToString();
                while (start-- > 0 && dr.Read()) ;

                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string link = "<a href=\"/Contests/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "\">";

                    sb.Append("<div class=\"coupon\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", Functions.ConvertToString(dr["Title"]), link);
                    sb.Append(link).AppendFormat("<div class=\"img\"><img src=\"{0}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagessweepcontesturl"] + Functions.ConvertToString(dr["Thumbnail"]));
                    if (dr["DateExpire"] != DBNull.Value)
                    {
                        DateTime expireDate = Convert.ToDateTime(dr["DateExpire"]);
                        sb.Append("<div class=\"expire\"><span style=\"font-style:italic\">Ends on</span> " + Functions.UppercaseFirst(expireDate.ToString("MMMM d")) + Functions.GetDateSuffix(expireDate) + ", " + expireDate.ToString("yyyy") + "</div>");
                    }
                    sb.AppendFormat("<div class=\"description\">{0}</div>", Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
                    sb.AppendFormat("<div class=\"buttons\">");
                    sb.AppendFormat("<div class=\"more_info\">{0}<img src=\"/images/more_info.png\" /></a></div>", link);
                    sb.AppendFormat("<div class=\"more_info\"><a target=\"_blank\" href=\"" + Convert.ToString(dr["Url"]) + "\"><img src=\"/images/enter.png\" /></a></div>", link);
                    sb.AppendFormat("</div>");

                    sb.Append("</div><div style=\"clear:both;\"></div><hr />");

                    litCoupons.Text = "<hr class=\"blueline\" />" + sb.ToString();
                }
                if (i > NB_PER_PAGE)
                    lnkNext.Visible = true;
            }
            dr.Close();
            dr.Dispose();
        }

    }
}