/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';

    config.toolbar = [
        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', "PasteFromWord", 'Undo', 'Redo', "Scayt"] },
        { name: 'font', items: ["NumberedList", "BulletedList", "CreateDiv", "Outdent", "Indent", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"] },
        { name: 'html', items: ["Link", "UnLink", "Image", "Flash", "HorizontalRule", "SpecialChar", "-"] },
        "/",
        { name: 'text', items: ['Bold', 'Italic', 'Underline', "TextColor", "BGColor", "RemoveFormat"] },
        { name: 'style', items: ['Styles', 'Format', 'Font', 'FontSize', 'lineheight'] },
        { name: 'Maximize', items: ["Maximize"] },
        { name: 'Source', items: ['Source'] },
        { name: 'Source', items: ["About"] }
    ];
};
