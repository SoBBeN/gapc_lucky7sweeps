﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;

namespace DB
{

    public class DbFunctions
    {
        private static int WebSiteID
        {
            get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["websiteid"]); }
        }

        public static DataRow Authenticate(string username, string password)
        {
            StringBuilder query = new StringBuilder("[BF_Admins_Authenticate] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(username)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password)));
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            //HttpContext.Current.Response.Write(query.ToString());
            return objSqlServer.GetFirstRow(query.ToString());
        }

        public static SqlDataReader GetAllLifestyle()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Get] ").Append("1,0");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllLifestyleSlide()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_Get] ").Append("1,0");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllLifestyleSlide(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_Get] ").Append("1,0," + lifestyleid.ToString());

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleSlide(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteLifestyle(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteLifestyleSlide(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleSlide_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyle(int id, string title,
                                         bool isActive, string activeDate, string subtitle)
        {
            StringBuilder query = new StringBuilder("[Lifestyle_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleSlide(int id, int lifestyleid, string imageFilename, string subtitle, string description,
                                         bool isActive, string activeDate, string thumbnail, string descriptiontop, bool imageVisible, string VideoScript)
        {
            StringBuilder query = new StringBuilder("[LifestyleSlide_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(descriptiontop)).Append(",");
            query.Append(objSqlServer.FormatSql(imageVisible)).Append(",");
            query.Append(objSqlServer.FormatSql(VideoScript));


            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetWebSites()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Website_GetAll] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetWebSite(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Website_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }


        public static int InsertLifestyle(string title,
                                         bool isActive, string activeDate, string subtitle)
        {
            StringBuilder query = new StringBuilder("[Lifestyle_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static int InsertLifestyleSlide(string imageFilename, int lifestyleid, string subtitle, string description,
                                         bool isActive, string activeDate, string thumbnail, string descriptiontop, bool imageVisible, string VideoScript)
        {
            StringBuilder query = new StringBuilder("[LifestyleSlide_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(subtitle)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(descriptiontop)).Append(",");
            query.Append(objSqlServer.FormatSql(imageVisible)).Append(",");
            query.Append(objSqlServer.FormatSql(VideoScript));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void MoveLifestyleSlide(int slideid, bool moveup)
        {
            StringBuilder query = new StringBuilder("[LifestyleSlide_Move] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(slideid)).Append(",");
            query.Append(objSqlServer.FormatSql(moveup));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetLifestyleWebSites()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_GetWebsites] ");
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleCategory(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Category_GetAll] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader LifestyleGetByCategory(int CategoryID)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Get_ByCategory_No_Website] ");
            query.Append(CategoryID);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetLifestyleCategoryDistinct()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Category_GetAll_Distinct] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void InsertLifestyleCategory(int lifestyleid, int category)
        {
            StringBuilder query = new StringBuilder("[LifestyleToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[Lifestyle_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }

        public static SqlDataReader GetLifestyleCategory()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleCategory_GetAll]");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetOneLifestyleCategory(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleCategory_GetOne] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteLifestyleCategory(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleCategory_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleCategory(int id, string description, int websiteid)
        {
            StringBuilder query = new StringBuilder("[LifestyleCategory_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertLifestyleCategory(string description, int websiteid)
        {
            StringBuilder query = new StringBuilder("[LifestyleCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(description));
            query.Append(",").Append(objSqlServer.FormatSql(websiteid));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }



        public static SqlDataReader GetLifestyleTag(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Lifestyle_Tag_GetAll] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void InsertLifestyleTag(int lifestyleid, int tag)
        {
            StringBuilder query = new StringBuilder("[LifestyleToTag_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(lifestyleid)).Append(",");
            query.Append(objSqlServer.FormatSql(tag));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetLifestyleTag()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleTag_GetAll]");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetOneLifestyleTag(int lifestyleid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleTag_GetOne] ");
            query.Append(lifestyleid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteLifestyleTag(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[LifestyleTag_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateLifestyleTag(int id, string description)
        {
            StringBuilder query = new StringBuilder("[LifestyleTag_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(description));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertLifestyleTag(string description)
        {
            StringBuilder query = new StringBuilder("[LifestyleTag_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(description));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }

        public static SqlDataReader GetRecipeCuisine(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Cuisine_GetAll] ");
            query.Append(recipeid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRecipeMeal(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Meal_GetAll] ");
            query.Append(recipeid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRecipeCategory(int recipeid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Category_GetAll] ");
            query.Append(recipeid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllRecipe()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Get] ").Append("1,0");

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetRecipe(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_GetOne] ").Append(objSqlServer.FormatSql(id));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteRecipe(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Recipe_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateRecipe(int id, string imageFilename, int cooktime, int preptime, int totaltime, int portions, string author, string title, string description, string ingredients,
                                         string method, bool isActiveAll, bool isActive, string activeDate, string authorlink, string authorimage)
        {
            StringBuilder query = new StringBuilder("[Recipe_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(cooktime)).Append(",");
            query.Append(objSqlServer.FormatSql(preptime)).Append(",");
            query.Append(objSqlServer.FormatSql(totaltime)).Append(",");
            query.Append(objSqlServer.FormatSql(portions)).Append(",");
            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(ingredients)).Append(",");
            query.Append(objSqlServer.FormatSql(method)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(authorlink)).Append(",");
            query.Append(objSqlServer.FormatSql(authorimage));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateRecipe(int id, int websiteid, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[Recipe_Update_Website] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));
            HttpContext.Current.Response.Write(query.ToString());
            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertRecipe(string imageFilename, int cooktime, int preptime, int totaltime, int portions, string author, string title, string description, string ingredients,
                                         string method, bool isActiveAll, bool isActive, string activeDate, string authorlink, string authorimage)
        {
            StringBuilder query = new StringBuilder("[Recipe_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(cooktime)).Append(",");
            query.Append(objSqlServer.FormatSql(preptime)).Append(",");
            query.Append(objSqlServer.FormatSql(totaltime)).Append(",");
            query.Append(objSqlServer.FormatSql(portions)).Append(",");
            query.Append(objSqlServer.FormatSql(author)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(ingredients)).Append(",");
            query.Append(objSqlServer.FormatSql(method)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate)).Append(",");
            query.Append(objSqlServer.FormatSql(authorlink)).Append(",");
            query.Append(objSqlServer.FormatSql(authorimage));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void InsertRecipeCategory(int recipeid, int category)
        {
            StringBuilder query = new StringBuilder("[RecipeToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(recipeid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void InsertRecipeCuisine(int recipeid, int cuisine)
        {
            StringBuilder query = new StringBuilder("[RecipeToCuisine_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(recipeid)).Append(",");
            query.Append(objSqlServer.FormatSql(cuisine));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void InsertRecipeMeal(int recipeid, int meal)
        {
            StringBuilder query = new StringBuilder("[RecipeToMeal_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(recipeid)).Append(",");
            query.Append(objSqlServer.FormatSql(meal));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertWebSite(string description, string disclaimer1, string disclaimer2, string privacy, string terms, string disclaimer3, string disclaimer4, string ContentHeader)
        {
            StringBuilder query = new StringBuilder("[Website_Insert_Content] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer1)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer2)).Append(",");
            query.Append(objSqlServer.FormatSql(privacy)).Append(",");
            query.Append(objSqlServer.FormatSql(terms)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer3)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer4)).Append(",");
            query.Append(objSqlServer.FormatSql(ContentHeader));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void UpdateWebSite(int id, string description, string disclaimer1, string disclaimer2, string privacy, string terms, string disclaimer3, string disclaimer4, string ContentHeader)
        {
            StringBuilder query = new StringBuilder("[Website_Update_Content] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(description)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer1)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer2)).Append(",");
            query.Append(objSqlServer.FormatSql(privacy)).Append(",");
            query.Append(objSqlServer.FormatSql(terms)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer3)).Append(",");
            query.Append(objSqlServer.FormatSql(disclaimer4)).Append(",");
            query.Append(objSqlServer.FormatSql(ContentHeader));

            objSqlServer.ExecNonQuery(query.ToString());
        }




        public static SqlDataReader GetAllContent()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_Get] ").Append("1,0");

            query.Append(',').Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetContent(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_GetOne] ").Append(objSqlServer.FormatSql(id));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void MoveContent(int id, bool moveup)
        {
            StringBuilder query = new StringBuilder("[Content_Move] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(moveup));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteContent(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[Content_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void DeleteContentImage(int id)
        {
            StringBuilder query = new StringBuilder("[Content_DeleteImage] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateContent(int id, string title, string category, string desctop, string descbtm, bool active, string imageFilename, string imageLink)
        {
            StringBuilder query = new StringBuilder("[Content_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(desctop)).Append(",");
            query.Append(objSqlServer.FormatSql(descbtm)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(',');
            query.Append(objSqlServer.FormatSql(imageLink));


            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertContent(string title, string category, string desctop, string descbtm, bool active, string imageFilename, string imageLink)
        {
            StringBuilder query = new StringBuilder("[Content_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(category)).Append(",");
            query.Append(objSqlServer.FormatSql(desctop)).Append(",");
            query.Append(objSqlServer.FormatSql(descbtm)).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(',');
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(',');
            query.Append(objSqlServer.FormatSql(imageLink));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }






        public static SqlDataReader GetAdmins()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Admins_GetAll] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAdmin(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Admins_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteAdmin(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Admins_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateAdmin(int id, string username, string password, bool active, bool editusers)
        {
            StringBuilder query = new StringBuilder("[BF_Admins_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(username)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(editusers));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertAdmin(string username, string password, bool active, bool editusers)
        {
            StringBuilder query = new StringBuilder("[BF_Admins_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(username)).Append(",");
            query.Append(objSqlServer.FormatSql(Functions.GetSha256(password))).Append(",");
            query.Append(objSqlServer.FormatSql(active)).Append(",");
            query.Append(objSqlServer.FormatSql(editusers));

            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }


        /*   Sweepstakes & Contest   */

        public static SqlDataReader GetSweepContestsCategoryAll()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_GetAll] ");

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContestsCategoryOne(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_GetOne] ").Append(objSqlServer.FormatSql(id));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteSweepContestCategory(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static SqlDataReader GetSweepContests(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_GetOne] ").Append(id);
            query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetAllSweepContests()
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Get] ");

            query.Append(objSqlServer.FormatSql(WebSiteID));
            return objSqlServer.GetDataReader(query.ToString());
        }
        public static SqlDataReader GetSweepContestsCategory(int sweepcontestsid)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Category_GetAll] ");
            query.Append(sweepcontestsid);

            return objSqlServer.GetDataReader(query.ToString());
        }
        public static void DeleteSweepContests(int id)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Delete] ").Append(objSqlServer.FormatSql(id));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSweepContests(int id, string title, string url, string nbPoints, string dateActive, string dateExpire, bool isActiveAll, bool isActive, string text, string imageFilename, string thumbnail, string name)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Update] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(nbPoints)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(dateActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSweepContestsThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void UpdateSweepContests(int id, int websiteid, bool isActive, string activeDate)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Update_Website] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(websiteid)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(activeDate));
            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int InsertSweepContests(string title, string url, string nbPoints, string dateActive, string dateExpire, bool isActiveAll, bool isActive, string text, string imageFilename, string thumbnail, string name)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(title)).Append(",");
            query.Append(objSqlServer.FormatSql(text)).Append(",");
            query.Append(objSqlServer.FormatSql(url)).Append(",");
            query.Append(objSqlServer.FormatSql(nbPoints)).Append(",");
            query.Append(objSqlServer.FormatSql(imageFilename)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail)).Append(",");
            query.Append(objSqlServer.FormatSql(dateExpire)).Append(",");
            query.Append(objSqlServer.FormatSql(isActiveAll)).Append(",");
            query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
            query.Append(objSqlServer.FormatSql(isActive)).Append(",");
            query.Append(objSqlServer.FormatSql(dateActive)).Append(",");
            query.Append(objSqlServer.FormatSql(name));

            return Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        public static void InsertPSweepContestsCategory(int sweepcontestsid, int category)
        {
            StringBuilder query = new StringBuilder("[SweepContestsToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(sweepcontestsid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static int SweepContestsInsertCategory(string category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Insert_Category] ").Append(objSqlServer.FormatSql(category));

            return int.Parse(objSqlServer.GetScalar(query.ToString()).ToString());
        }
        public static void SweepContestsUpdateCategory(int id, string category)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[SweepContests_Update_Category] ").Append(objSqlServer.FormatSql(id)).Append(",").Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void InsertSweepContestsCategory(int pointsOffersid, int category)
        {
            StringBuilder query = new StringBuilder("[SweepContestsToCategory_Insert] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(pointsOffersid)).Append(",");
            query.Append(objSqlServer.FormatSql(category));

            objSqlServer.ExecNonQuery(query.ToString());
        }
        public static void SweepContestsImageUpdateThumbnail(int id, string thumbnail)
        {
            StringBuilder query = new StringBuilder("[SweepContests_Image_Update_Thumbnail] ");
            SqlServer objSqlServer = new SqlServer();

            query.Append(objSqlServer.FormatSql(id)).Append(",");
            query.Append(objSqlServer.FormatSql(thumbnail));

            objSqlServer.ExecNonQuery(query.ToString());
        }
    }
}