﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Admins.aspx.cs" Inherits="Admins" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3><%=ITEMNAME %></h3>
    <br />
    <a href="AdminsAdd.aspx">Add New <%=ITEMNAME %></a>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <asp:GridView ID="gv" runat="server" Width="100%" GridLines="None" DataKeyNames="ID"
        OnRowDeleting="GvRowDeleting" CssClass="grid">
        <HeaderStyle CssClass="gridHead" />
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="AdminsAdd.aspx?id={0}"
                Text="Edit" />
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Delete"
                        Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this user?')" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

</asp:Content>

