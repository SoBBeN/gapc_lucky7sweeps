﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebsiteAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Website";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EditUsers"] == null || Convert.ToBoolean(Session["EditUsers"]) != true)
        {
            Response.Write("You don't have access to this section");
            Response.End();
            return;
        }

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetWebSite(id);

        if (dr.HasRows)
        {
            dr.Read();
            txtDescription.Text = Convert.ToString(dr["Description"]);
            txtDisclaimer1.Text = Convert.ToString(dr["Disclaimer1"]);
            txtDisclaimer2.Text = Convert.ToString(dr["Disclaimer2"]);
            txtPrivacy.Text = Convert.ToString(dr["Privacy"]);
            txtTerms.Text = Convert.ToString(dr["Terms"]);
            txtContentHeader.Text = Convert.ToString(dr["ContentHeader"]);
            btnSave.Text = "Update " + ITEMNAME;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

                int id = 0;

                if (hidID.Value.Length == 0) //INSERT
                {
                    id = DB.DbFunctions.InsertWebSite(txtDescription.Text, txtDisclaimer1.Text, txtDisclaimer2.Text, txtPrivacy.Text, txtTerms.Text, txtDisclaimer3.Text, txtDisclaimer4.Text, txtContentHeader.Text);

                    //POLL INSERTED SUCCESSFULLY
                    if (id > 0)
                    {
                        hidID.Value = id.ToString();
                        divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                        divMsg.Visible = true;
                    }
                }
                else //UPDATE
                {
                    id = int.Parse(hidID.Value);
                    DB.DbFunctions.UpdateWebSite(id, txtDescription.Text, txtDisclaimer1.Text, txtDisclaimer2.Text, txtPrivacy.Text, txtTerms.Text, txtDisclaimer3.Text, txtDisclaimer4.Text, txtContentHeader.Text);

                    divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                    divMsg.Visible = true;

                }
                //ShowExistingValues();
        }
    }
}