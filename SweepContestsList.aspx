﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SweepContestsList.aspx.cs" Inherits="SweepContestsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <link href="/css/SweepContests.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ContentPlaceHolder1_divSectionSweeps").hide();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <h1>Sweepstakes & Contests</h1>
    <div id="coupons">
        <asp:Literal runat="server" ID="litCoupons"></asp:Literal>
    </div>
    <div id="poPrevNext">
        <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< Previous</a></div>
        <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">Next ></a></div>
        <div style="clear:both;"></div>
    </div>
    <br /><br />
</asp:Content>