﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LifestyleTag.aspx.cs" Inherits="LifestyleTag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <link href="/css/lifestyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <h1><asp:Literal runat="server" ID="litcategory"></asp:Literal></h1>
    <hr class="blueline" />
    <div id="lifestyles">
        <asp:Literal runat="server" ID="litArticles"></asp:Literal>
    </div>
    <div id="poPrevNext">
        <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< Previous</a></div>
        <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">Next ></a></div>
        <div style="clear:both;"></div>
    </div>
    <br /><br />
</asp:Content>