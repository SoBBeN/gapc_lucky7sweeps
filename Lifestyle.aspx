﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Lifestyle.aspx.cs" Inherits="Lifestyle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <link href="/css/lifestyle.css" rel="stylesheet" />

    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({ publisher: "98c30075-93f7-4baa-9669-a5a1792d12a3", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
    <script type="text/javascript">
        var currentslide = <%=slideshown %>;
        var maxslide = <%=nbslide %>;
        function nextslide() {
            if (currentslide == maxslide)
                window.location = "<%=redirect %>";
            else
                changeslide(currentslide + 1);
        }
        function prevslide() {
            if (currentslide == 1)
                changeslide(maxslide);
            else
                changeslide(currentslide - 1);
        }
        function changeslide(newslide) {
            document.getElementById("currentslide2").innerHTML = newslide;
            document.getElementById("slide" + currentslide).style.display = "none";
            document.getElementById("dot" + currentslide).className  = "";
            currentslide = newslide;
            document.getElementById("slide" + currentslide).style.display = "block";
            document.getElementById("dot" + currentslide).className = "selected";
            googletag.pubads().refresh();
        }

    </script>

    <asp:Literal runat="server" ID="litPixel"></asp:Literal>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div id="lifestyle">
        <div class="main">
            <div class="title"><asp:Literal runat="server" ID="litTitle"></asp:Literal></div>
            <div class="subtitle"><asp:Literal runat="server" ID="litSubTitle"></asp:Literal></div>

            <div style="clear:both;"></div>
            <div id="ad" style="width:311px;">
                
            </div>
            <div id="slides" class="ckedit">
                <asp:Literal ID="litSlides" runat="server"></asp:Literal>
            </div>
            <br />
            <div id="dots"><asp:Literal ID="litDots" runat="server"></asp:Literal></div>
            <div class="poPrevNext">
                <div class="prev lnk"><a href="javascript:void(0);" onclick="prevslide();" runat="server">< Previous</a></div>
                <div class="next lnk"><a href="javascript:void(0);" onclick="nextslide();" runat="server">Next ></a></div>
                <span id="currentslide2"><%=slideshown %></span> of <%=nbslide %>
                <div style="clear:both;"></div>
            </div>
            <div style="clear:both;"></div>
            <div class="tags"><span class="title">Topics:</span> <asp:Literal runat="server" ID="litTags"></asp:Literal></div>
            <br />
        </div>
        <div style="clear:both;"></div>
        <br /><br />
    </div>
</asp:Content>