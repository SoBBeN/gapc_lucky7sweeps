﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : ITmgMasterPage
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("\n    <title>");
        if (pagetitle != System.Configuration.ConfigurationManager.AppSettings["title"])
            sb.Append(System.Configuration.ConfigurationManager.AppSettings["title"] + " - ");
        sb.Append(pagetitle);
        sb.AppendLine("</title>");
        sb.Append("    <link rel=\"image_src\" href=\"").Append(HttpUtility.UrlPathEncode(pagelogo)).AppendLine("\" />");

        if (showOGtags)
        {
            sb.Append("    <meta property=\"og:title\" content=\"").Append(pagetitle).AppendLine("\" />");
            sb.Append("    <meta property=\"og:url\" content=\"").Append(pageurl).AppendLine("\" />");
            sb.Append("    <meta property=\"og:image\" content=\"").Append(HttpUtility.UrlPathEncode(pagelogo)).AppendLine("\" />\n");
            sb.Append("    <meta property=\"og:type\" content=\"").Append(pagetype).AppendLine("\" />\n");
            if (pagedescription.Length > 0)
            {
                sb.Append("    <meta property=\"og:description\" content=\"").Append(pagedescription).AppendLine("\" />\n");
                sb.Append("    <meta name=\"description\" content=\"").Append(pagedescription).AppendLine("\" />\n");
            }
            foreach (string img in pageimg)
            {
                sb.Append("    <meta property=\"og:image\" content=\"").Append(HttpUtility.UrlPathEncode(img)).AppendLine("\" />\n");
            }
        }
        lithead.Text = sb.ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FillMenuWithLifestyle();
    }

    private void FillMenuWithLifestyle()
    {
        StringBuilder sb = new StringBuilder();

        SqlDataReader dr = Functions.GetAllLifestyleCategories();
        if (dr != null && dr.HasRows)
        {
            while (dr.Read())
            {
                sb.Append("<li><a href=\"/Lifestyle/");
                sb.Append(dr["ID"]);
                sb.Append("/");
                sb.Append(Functions.StrToURL(Convert.ToString(dr["Description"])));
                sb.Append("/");
                sb.Append("\">");
                sb.Append(Convert.ToString(dr["Description"]));
                sb.Append("</a></li>");
            }
            dr.Close();
            dr.Dispose();
        }
        //litLifestyle.Text = sb.ToString();
    }
}
