﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RegisterRoutes(System.Web.Routing.RouteTable.Routes);
    }

    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
        string currentUrl = HttpContext.Current.Request.Url.ToString().ToLower();
        if (currentUrl.StartsWith("http://www."))
        {
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", currentUrl.Replace("http://www.", "http://"));
            Response.End();
        }
    }

    public static void RegisterRoutes(System.Web.Routing.RouteCollection routes)
    {
        routes.MapPageRoute("404",
            "404",
            "~/404.aspx");
        routes.MapPageRoute("Error",
            "Error",
            "~/Error.aspx");
        routes.MapPageRoute("SiteMap",
            "Sitemap",
            "~/sitemap.aspx");
        routes.MapPageRoute("Content",
            "Content/{id}/{name}",
            "~/Content.aspx");
        routes.MapPageRoute("LifestyleC",
            "Lifestyle",
            "~/LifestyleCategories.aspx");
        //routes.MapPageRoute("LifestyleCs",
        //    "Lifestyle/{id}/{title}",
        //    "~/LifestyleCategories.aspx");
        //routes.MapPageRoute("Lifestyle",
        //    "Lifestyle/{id}/{category}/{title}",
        //    "~/Lifestyle.aspx");
        //routes.MapPageRoute("LifestyleSlide",
        //    "Lifestyle/{id}/{category}/{title}/{slideid}",
        //    "~/Lifestyle.aspx");
        //routes.MapPageRoute("LifestyleTag",
        //    "LifestyleTag/{id}/{tag}",
        //    "~/LifestyleTag.aspx");
        routes.MapPageRoute("LifestyleCs",
            "Lifestyle/{id}/{title}",
            "~/404.aspx");
        routes.MapPageRoute("Lifestyle",
            "Lifestyle/{id}/{category}/{title}",
            "~/404.aspx");
        routes.MapPageRoute("LifestyleSlide",
            "Lifestyle/{id}/{category}/{title}/{slideid}",
            "~/404.aspx");
        routes.MapPageRoute("LifestyleTag",
            "LifestyleTag/{id}/{tag}",
            "~/404.aspx");
        routes.MapPageRoute("Contest",
            "Contests",
            "~/SweepContestsList.aspx");
        routes.MapPageRoute("Contests",
            "Contests/{id}/{title}",
            "~/SweepContests.aspx");
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

</script>
