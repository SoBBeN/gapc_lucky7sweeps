﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 1;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            btnSubmit.ImageUrl = "images/subscribe.png";

            ((ITmgMasterPage)Master).PageDescription = System.Configuration.ConfigurationManager.AppSettings["description"];
            ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["logo"];

            int id;
            id = -2147483648;

            SqlDataReader dr;
            dr = Functions.GetLifestyle(id);
            
            if (dr.HasRows)
            {
                dr.NextResult();
                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string url = "Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(dr["CategoryDescription"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);
                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["Description"])), 150).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append("<div class=\"img\">").Append(link).AppendFormat("<img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"], Functions.ConvertToString(dr["Thumbnail"]));
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append(description);
                    sb.Append("<div class=\"buttons\">");
                    sb.AppendFormat("<div class=\"more_info\">{0}<img src=\"images/more_info.png\" /></a></div>", link);
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                }

                //litArticles.Text = sb.ToString();
            }

            dr.Close();
            dr.Dispose();

            dr = Functions.GetSweepContestsList(-1);

            if (dr.HasRows)
            {
                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string url = "Contests/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);

                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 150).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append("<div class=\"img\">").Append(link).AppendFormat("<img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagessweepcontesturl"], Functions.ConvertToString(dr["Thumbnail"]));
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append(description);
                    sb.Append("<div class=\"buttons\">");
                    sb.AppendFormat("<div class=\"more_info\">{0}<img src=\"images/more_info.png\" /></a></div>", link);
                    sb.AppendFormat("<div class=\"more_info\"><a target=\"_blank\" href=\"" + Convert.ToString(dr["Url"]) + "\"><img src=\"images/enter.png\" /></a></div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");

                    litSweeps.Text = sb.ToString();
                }
                dr.Close();
                dr.Dispose();
            }

            dr = Functions.GetContent(175);

            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    StringBuilder sb = new StringBuilder();
                    string url = "Content/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);

                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["DescriptionBtm"])), 150).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append("<div class=\"img\">").Append(link).AppendFormat("<img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/content/", Functions.ConvertToString(dr["ImageFileName"]));
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append(description);
                    sb.Append("<div class=\"buttons\">");
                    sb.AppendFormat("<div class=\"more_info\">{0}<img src=\"images/more_info.png\" /></a></div>", link);
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");

                    litCoupon.Text = sb.ToString();
                }
                dr.Close();
                dr.Dispose();
            }

            dr = Functions.GetContent(176);

            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    StringBuilder sb = new StringBuilder();
                    string url = "Content/" + Convert.ToString(dr["ID"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);

                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["DescriptionBtm"])), 150).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append("<div class=\"img\">").Append(link).AppendFormat("<img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/content/", Functions.ConvertToString(dr["ImageFileName"]));
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    sb.Append(description);
                    sb.Append("<div class=\"buttons\">");
                    sb.AppendFormat("<div class=\"more_info\">{0}<img src=\"images/more_info.png\" /></a></div>", link);
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");

                    litFiancial.Text = sb.ToString();
                }
            dr.Close();
            dr.Dispose();
            }

        }
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e)
    {

       
        //ClientScript.RegisterClientScriptBlock(this.GetType(), "Formsubmitted", "alert(\"You're information has been submitted.\");", true);

        Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "Contests?post=yes");  
        //txtiliketodrinkatlunch.Text = "";
        chkDailyEmail.Checked = false;
        chkPrivacy.Checked = false;


        //Post to AIB api
        //string apiURL = "https://api2.all-inbox.com";
        //string api_key = "2a03402d_a4da_4dc6_9796_ab96dbd20dd0";
        //int source_id = 789;
        //string email = txtiliketodrinkatlunch.Text;
        //string ip_address = HttpContext.Current.Request.UserHostAddress;
        //string source_sub_id = "organic";
        //string referrer = "";


        //if (Request.QueryString["referrer"] == "facebook")
        //{
            
        //    referrer = Request.QueryString["referrer"];
        //    Session["twm_referrer"] = referrer;
        //    source_sub_id = referrer;
        //}

        //string param = "resource=contacts" + "&api_key=" + api_key + "&source_id=" + source_id + "&email=" + email + "&ip_address=" + ip_address + "&source_sub_id=" + source_sub_id;
        //DoPost(apiURL, param);
    }

    public static string DoPost(string postToURL,string param)
    {
        string contentType = "application/x-www-form-urlencoded";
       

        //Post Method Logic
        HttpWebRequest sendRequest = default(HttpWebRequest);
        sendRequest = (HttpWebRequest)WebRequest.Create(postToURL);
        sendRequest.Method = "POST";

  
        sendRequest.ContentType = contentType;



        //Define content length
        sendRequest.ContentLength = param.Length;

        //Send Request
        StreamWriter myWriter = null;
        try
        {
            myWriter = new StreamWriter(sendRequest.GetRequestStream());
            myWriter.Write(param);
            myWriter.Flush();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if ((myWriter != null))
            {
                myWriter.Close();
            }
        }

        //Receive response
        //Receive response
        StreamReader reader = null;
        string responseFromPartner = null;
        StringBuilder errorText = new StringBuilder();
        try
        {
            try
            {
                if ((sendRequest.GetResponse().Headers != null))
                {
                    errorText.Append("Headers:" + Environment.NewLine);
                    foreach (string headerName in sendRequest.GetResponse().Headers)
                    {
                        errorText.Append(headerName + ":" + sendRequest.GetResponse().Headers.Get(headerName) + Environment.NewLine);
                    }
                    errorText.Append(Environment.NewLine);
                }
                errorText.AppendLine("ContentLength:" + sendRequest.GetResponse().ContentLength);
                errorText.AppendLine("ContentType:" + sendRequest.GetResponse().ContentType);

            }
            catch
            {
            }
            reader = new StreamReader(sendRequest.GetResponse().GetResponseStream());
            responseFromPartner = reader.ReadToEnd();
        }
        catch (WebException webex)
        {
            throw new WebException(webex.Message + Environment.NewLine + errorText.ToString(), webex.InnerException, webex.Status, webex.Response);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if ((reader != null))
            {
                reader.Close();
            }
        }


        
        return responseFromPartner;
    }
}