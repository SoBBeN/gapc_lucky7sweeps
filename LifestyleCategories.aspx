﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LifestyleCategories.aspx.cs" Inherits="LifestyleCategories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <link href="/css/lifestyle.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ContentPlaceHolder1_divSectionLifeStyle").hide();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div id="content">
        <h1><asp:Literal runat="server" ID="litcategory"></asp:Literal></h1>
        <hr class="blueline" />
        <div id="lifestyles">
            <asp:Literal runat="server" ID="litArticles"></asp:Literal>
        </div>
        <div id="poPrevNext">
            <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< Previous</a></div>
            <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">Next ></a></div>
            <div style="clear:both;"></div>
        </div>
    </div>
</asp:Content>