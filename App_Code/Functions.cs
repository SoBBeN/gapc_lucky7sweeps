﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

public class Functions
{
    private static int WebSiteID
    {
        get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["websiteid"]); }
    }

    public static SqlDataReader GetWebSite()
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[Website_GetOne] ");

        query.Append(objSqlServer.FormatSql(WebSiteID));
        return objSqlServer.GetDataReader(query.ToString());
    }

    public static string StrToURL(object obj)
    {
        return StrToURL(Convert.ToString(obj));
    }

    public static string StrToImgUrl(string ImgName, string Title) // added by Adam
    {
        string Category = Regex.Match(Title, @"^(\w+\b.*?){1}").ToString();

        var CategoryArray = new Dictionary<string, string>
        { 
           { "Products", "Product" },
           { "Photos", "images" },
           { "Today", "Todaysmom" }
          
        };

        foreach (KeyValuePair<string, string> item in CategoryArray)
        {

            if (Category == item.Key)
            {
                Category = item.Value;
            }

        }
        string ImgUrl = "images/" + Category + "/" + ImgName;

        return ImgUrl;
    }
    public static string RemoveSpace(string Str) // added by Adam
    {

        return Str.Replace(' ', '_').Replace('\'', '_').Replace('\"', '_');
    }
    public static string StrToURL(string str)
    {
        return str.Replace("?", "").Replace("“", "").Replace("”", "").Trim().Replace(' ', '_').Replace('.', '_').Replace('…', '_').Replace(':', '_').Replace("&", "And").Replace("+", "And").Replace("/", "_And_").Replace("%", "_").Replace("\"", "");
    }

    public static string RemoveSpecialChars(string str)
    {
        return str.Replace("?", "").Replace("“", "").Replace('…', '_').Replace(':', '_').Replace("&", "And").Replace("#", "");
    }

    public static string RemoveHtml(string str)
    {
        str = str.Replace("&nbsp;", " ");
        str = Regex.Replace(str, "<.*?>", " ").Replace('\n', ' ').Replace('\r', ' ').Replace('<', ' ').Replace('>', ' ');
        RegexOptions options = RegexOptions.None;
        Regex regex = new Regex(@"[ ]{2,}", options);
        str = regex.Replace(str, @" ");
        return str;
    }

    public static string ShortenText(string str, short length)
    {
        if (str.Length > length)
        {
            str = str.Substring(0, length);
            str = str.Substring(0, str.LastIndexOf(" ")) + " ...";
        }
        return str;
    }

    public static string ConvertToString(object obj)
    {
        if (DBNull.Value == obj)
            return string.Empty;
        else if (null == obj)
            return string.Empty;
        else
            return Convert.ToString(obj);
    }

    //Function to get random number
    private static readonly Random getrandom = new Random();
    private static readonly object syncLock = new object();
    public static int GetRandomNumber(int min, int max)
    {
        lock (syncLock)
        { // synchronize
            return getrandom.Next(min, max);
        }
    }

    public static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }

    public static bool isMobile()
    {
        string u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
        Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);

        try
        {
            if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
            {
                return true;
            }
        }
        catch (Exception ex)
        {
            return false;
        }

        return false;
    }

    public static string GetDateSuffix(DateTime date)
    {
        // Get day...
        int day = date.Day;

        // Get day modulo...
        int dayModulo = day % 10;

        // Convert day to string...
        string suffix;

        // Combine day with correct suffix...
        suffix = (day == 11 || day == 12 || day == 13) ? "th" :
            (dayModulo == 1) ? "st" :
            (dayModulo == 2) ? "nd" :
            (dayModulo == 3) ? "rd" :
            "th";

        // Return result...
        return suffix;
    }

    public static SqlDataReader GetLifestyle(int id)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[Lifestyle_GetList] ");

        query.Append(objSqlServer.FormatSql(id)).Append(",");
        query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
        query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetLifestyleRandom()
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[Lifestyle_GetRandom] ");

        query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
        query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetLifestyleTag(int id)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[LifestyleTag_GetList] ");

        query.Append(objSqlServer.FormatSql(id)).Append(",");
        query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
        query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetSweepContestsCategories(int pointsOffersid)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[SweepContests_Category_Get] ");
        query.Append(pointsOffersid);

        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetSweepContests(int id)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[SweepContests_GetOne] ");
        query.Append(objSqlServer.FormatSql(id)).Append(",");
        query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
        query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetSweepContestsList(int id)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[SweepContests_GetList] ");

        query.Append(objSqlServer.FormatSql(id)).Append(",");
        query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
        query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetSweepContestsPrevNext(int id)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[SweepContests_GetPrevNext] ");

        query.Append(objSqlServer.FormatSql(id)).Append(",");
        query.Append(objSqlServer.FormatSql(WebSiteID)).Append(",");
        query.Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);

        return objSqlServer.GetDataReader(query.ToString());
    }

    public static SqlDataReader GetContent(int id)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[Content_GetOne] ").Append(objSqlServer.FormatSql(id));

        query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetLifestyleWithSlides(int id, string category)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[LifestyleWithSlides] ").Append(objSqlServer.FormatSql(id));

        query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));
        query.Append(",").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);
        query.Append(",").Append(objSqlServer.FormatSql(category));
        return objSqlServer.GetDataReader(query.ToString());
    }

    public static SqlDataReader GetLifestyleTags(int id)
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[Lifestyle_GetLifestyleTags] ");

        query.Append(objSqlServer.FormatSql(id));
        return objSqlServer.GetDataReader(query.ToString());
    }
    public static SqlDataReader GetAllLifestyleCategories()
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[LifestyleCategories_GetActive] ").Append(System.Configuration.ConfigurationManager.AppSettings["showInactiveData"]);
        query.Append(",").Append(objSqlServer.FormatSql(WebSiteID));

        return objSqlServer.GetDataReader(query.ToString());
    }
}