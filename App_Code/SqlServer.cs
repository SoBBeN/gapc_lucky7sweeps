﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.Data.SqlClient;

public class SqlServer
{
    private string m_sConnectionString = String.Empty;
    private const short MAX_RETRIES = 3;

    private int m_iTimeout = 120;
    public int Timeout
    {
        get { return m_iTimeout; }

        set { m_iTimeout = value; }
    }

    public string ConnectionString
    {
        get { return m_sConnectionString; }

        set { m_sConnectionString = value; }
    }

    public SqlServer(string p_sConnectionString)
    {
        this.ConnectionString = p_sConnectionString;
    }

    public SqlServer()
    {
        this.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ConnectionString + ";Connect Timeout=125";
    }


    public enum ExecCommandType
    {
        SelectCommand,
        InserCommand,
        UpdateCommand,
        DeleteCommand
    }

    public SqlConnection GetNewConnection()
    {
        SqlConnection c = null;

        bool timeout = true;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                c = new SqlConnection(this.ConnectionString);
                c.Open();
                timeout = false;
            }
            catch (Exception ex)
            {
                if (c != null)
                    c.Dispose();

                if (retry > MAX_RETRIES || !IsTimeout(ex.Message))
                    throw ex;
            }
        }
        return c;
    }

    public virtual long ExecNonQuery(string psCommandText)
    {
        return ExecNonQuery(psCommandText, null);
    }

    public virtual long ExecNonQuery(string psCommandText, SqlConnection Connection)
    {
        SqlCommand oCmd = null;

        bool timeout = true;
        long iRecAffected = 0;

        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                if (Connection == null || Connection.State != ConnectionState.Open)
                    Connection = GetNewConnection();
                oCmd = new SqlCommand(psCommandText, Connection);
                oCmd.CommandTimeout = m_iTimeout;
                iRecAffected = oCmd.ExecuteNonQuery();
                timeout = false;
            }
            catch (Exception ex)
            {
                if (retry > MAX_RETRIES || !IsTimeout(ex.Message))
                    throw ex;
            }
            finally
            {
                if (oCmd != null)
                {
                    if (oCmd.Connection != null)
                    {
                        oCmd.Connection.Close();
                        oCmd.Connection.Dispose();
                    }
                    oCmd.Dispose();
                }
            }
        }

        return iRecAffected;
    }

    public virtual SqlDataReader GetDataReader(string psCommandText)
    {
        return GetDataReader(psCommandText, null);
    }

    public virtual SqlDataReader GetDataReader(string psCommandText, SqlConnection Connection)
    {
        SqlCommand oCmd = null;

        bool timeout = true;
        SqlDataReader sqlReader = null;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                if (Connection == null || Connection.State != ConnectionState.Open)
                    Connection = GetNewConnection();
                oCmd = new SqlCommand(psCommandText, Connection);
                oCmd.CommandTimeout = m_iTimeout;
                sqlReader = oCmd.ExecuteReader(CommandBehavior.CloseConnection);
                timeout = false;
            }
            catch (Exception ex)
            {
                if (oCmd != null)
                {
                    if (oCmd.Connection != null)
                    {
                        oCmd.Connection.Close();
                        oCmd.Connection.Dispose();
                    }
                    oCmd.Dispose();
                }

                if (retry > MAX_RETRIES || !IsTimeout(ex.Message))
                    throw ex;
            }
        }

        return sqlReader;
    }

    public virtual object GetScalar(string psCommandText)
    {
        SqlCommand oCmd = null;

        object oScalar = null;

        bool timeout = true;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                oCmd = new SqlCommand(psCommandText, GetNewConnection());
                oCmd.CommandTimeout = m_iTimeout;
                oScalar = oCmd.ExecuteScalar();
                timeout = false;
            }
            catch (Exception sqlEx)
            {
                if (retry > MAX_RETRIES || !IsTimeout(sqlEx.Message))
                    throw sqlEx;
            }
            finally
            {
                if (oCmd != null)
                {
                    if (oCmd.Connection != null)
                    {
                        oCmd.Connection.Close();
                        oCmd.Connection.Dispose();
                    }
                    oCmd.Dispose();
                }
            }
        }
        return oScalar;
    }

    public virtual DataSet GetDataSet(string psCommandText, SqlConnection Connection)
    {
        SqlCommand sqlCommand = null;
        SqlDataAdapter oDa = null;

        //SqlDataAdapter oDa = new SqlDataAdapter(psCommandText, ConnectionString);
        DataSet oDs = null;


        bool timeout = true;
        short retry = 0;
        while (timeout && retry++ <= MAX_RETRIES)
        {
            try
            {
                if (Connection == null || Connection.State != ConnectionState.Open)
                    Connection = GetNewConnection();
                sqlCommand = new SqlCommand(psCommandText, Connection);
                sqlCommand.CommandTimeout = m_iTimeout;
                oDa = new SqlDataAdapter(sqlCommand);
                oDa.SelectCommand.CommandTimeout = m_iTimeout;

                oDs = new DataSet();
                oDa.Fill(oDs);
                timeout = false;
            }
            catch (Exception sqlEx)
            {
                if (retry > MAX_RETRIES || !IsTimeout(sqlEx.Message))
                    throw sqlEx;
            }
            finally
            {
                if (sqlCommand != null)
                {
                    if (sqlCommand.Connection != null)
                    {
                        sqlCommand.Connection.Close();
                        sqlCommand.Connection.Dispose();
                    }
                    sqlCommand.Dispose();
                }
                if (oDa != null)
                    oDa.Dispose();
            }
        }
        return oDs;

    }

    public static bool IsTimeout(string msg)
    {
        return (msg.Contains("Timeout") || msg.Contains("transport-level error has occurred") || msg.Contains("connection was forcibly closed") || msg.Contains("The specified network name is no longer available"));
    }

    public virtual DataTable GetDataTable(string SQL)
    {
        return GetDataTable(SQL, null);
    }

    public virtual DataTable GetDataTable(string psCommandText, SqlConnection Connection)
    {
        DataSet oDs = GetDataSet(psCommandText, Connection);

        if (oDs == null)
        {
            return null;
        }
        else if (oDs.Tables.Count > 0)
        {
            return oDs.Tables[0];
        }
        else
        {
            return null;
        }
    }

    public virtual DataSet GetDataSet(string SQL)
    {
        return GetDataSet(SQL, null);
    }

    public virtual DataRow GetFirstRow(string psCommandText)
    {

        DataTable oDt = GetDataTable(psCommandText);

        if (oDt == null)
        {
            return null;
        }
        else if (oDt.Rows.Count > 0)
        {
            return oDt.Rows[0];
        }
        else
        {
            return null;
        }

    }

    public virtual string FormatSql(bool psParam)
    {
        if (psParam)
            return "1";
        else
            return "0";
    }

    public virtual string FormatSql(string psParam)
    {
        return FormatSql(psParam, false, true);
    }

    public virtual string FormatSql(int psParam)
    {
        return FormatSql(psParam, true, true);
    }

    public virtual string FormatSql(decimal psParam)
    {
        return FormatSql(psParam, false, true);
    }

    public virtual string FormatSql(string psParam, bool isNumber)
    {
        return FormatSql(psParam, isNumber, true);
    }

    public virtual string FormatSql(object psParam, bool isNumber, bool emptyReturnsNull)
    {
        DateTime dt;

        if (Convert.IsDBNull(psParam))
        {
            return "NULL";
        }
        else if (psParam == null)
        {
            return "NULL";
        }
        else if (Convert.ToString(psParam) == Convert.ToString(int.MinValue))
        {
            return "NULL";
        }
        else if (psParam.ToString().Length == 0)
        {
            if (emptyReturnsNull)
                return "NULL";
            else
                return "''";
        }
        else if (DateTime.TryParse(psParam.ToString(), out dt) && dt == DateTime.MinValue)
        {
            return "NULL";
        }
        else if (isNumber)
        {
            return Convert.ToString(psParam);
        }
        else if (!isNumber)
        {
            return "'" + Convert.ToString(psParam).Replace("'", "''") + "'";
        }
        else
        {
            return string.Empty;
        }
    }

    public virtual int ExecuteDataAdapter(ref SqlDataAdapter poSqlDataAdapter, ref DataSet poDataSet, ExecCommandType piCommandType)
    {
        try
        {
            int iRecAffected = 0;
            System.Data.SqlClient.SqlCommandBuilder oCmdBuilder = new System.Data.SqlClient.SqlCommandBuilder(poSqlDataAdapter);
            switch (piCommandType)
            {
                case ExecCommandType.DeleteCommand:
                    poSqlDataAdapter.DeleteCommand = oCmdBuilder.GetDeleteCommand();

                    break;
                case ExecCommandType.InserCommand:
                    poSqlDataAdapter.InsertCommand = oCmdBuilder.GetInsertCommand();

                    break;
                case ExecCommandType.UpdateCommand:
                    poSqlDataAdapter.UpdateCommand = oCmdBuilder.GetUpdateCommand();

                    break;
                case ExecCommandType.SelectCommand:
                    if (poSqlDataAdapter.SelectCommand.CommandText == string.Empty)
                    {
                        throw new Exception("Missing SelectCommad. Initilize poSqlDataAdapter.SelectCommand first!");
                    }
                    break;
            }

            if (piCommandType == ExecCommandType.SelectCommand)
            {
                iRecAffected = poSqlDataAdapter.Fill(poDataSet);
            }
            else
            {
                iRecAffected = poSqlDataAdapter.Update(poDataSet);
            }
            return iRecAffected;
        }
        catch (Exception Err)
        {
            throw new Exception(Err.Message, Err);
        }
    }

    public virtual int ExecuteDataAdapter(ref SqlDataAdapter poSqlDataAdapter, ref DataSet poDataSet, ExecCommandType piCommandType, string psCommandText)
    {
        int iRecAffected = 0;

        if (piCommandType != ExecCommandType.SelectCommand)
        {
            poSqlDataAdapter.Fill(poDataSet);
        }

        switch (piCommandType)
        {
            case ExecCommandType.DeleteCommand:
                poSqlDataAdapter.DeleteCommand = new SqlCommand(psCommandText);
                break;
            case ExecCommandType.InserCommand:
                poSqlDataAdapter.InsertCommand = new SqlCommand(psCommandText);
                break;
            case ExecCommandType.UpdateCommand:
                poSqlDataAdapter.UpdateCommand = new SqlCommand(psCommandText);
                break;
            case ExecCommandType.SelectCommand:
                poSqlDataAdapter.SelectCommand = new SqlCommand(psCommandText);
                break;
        }

        if (piCommandType == ExecCommandType.SelectCommand)
        {
            iRecAffected = poSqlDataAdapter.Fill(poDataSet);
        }
        else
        {
            iRecAffected = poSqlDataAdapter.Update(poDataSet);
        }

        return iRecAffected;

    }
}
