﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="thank-you.aspx.cs" Inherits="thank_you" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lucky7Sweeps</title>
        <meta name="keywords" content="class,action,guide, class action" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    
    <script type="text/javascript" src="/script/menu.js"></script>
        <link href="/css/style.css" rel="stylesheet" />
        <link href="/css/menu.css" rel="stylesheet" />

    <style>
         html, body {
            height: 100%;
            margin: 0;
         }
         .wrapper {
            min-height: 100%;

            /* Equal to height of footer */
            /* But also accounting for potential margin-bottom of last child */
            margin-bottom: -50px;
        }
         footer,.push {
            height: 50px;
            margin:0;
            padding:0;
         }
         footer .copy {
            padding-top: 20px;
         }
        .thankYou_wrapper {
                margin: 5% auto;
                max-width: 1150px;
                padding: 10px 15px;
        }
        .thankYouMsg {
            font-size: 20px;
            margin-bottom:2%;
            color: #5b657b;
        }
        .startHere {
            background-color: #4cd3ea;
            padding:15px 40px 10px
        }
         .startHere span:hover {
             color: #5b657b;
        }
        .startHere span {
            font-size: 21px;
            color: #ffffff;
            -webkit-transition: .50s;
            -moz-transition: .50s;
            -o-transition: .50s;
            transition: .50s;
            -webkit-backface-visibility: hidden;
            text-decoration: none;
        }
        .mobileOnly {
            display:none;
        }
        @media(max-width:700px) {
        .mobileOnly {
            display:block;
         }
        .thankYouMsg {
            text-align:center;
        }
         .startHere_div {
            text-align:center;
            margin-top: 5%;
        }
        }
       @media(max-width:420px) {

        .thankYouMsg {
            font-size:17px;
        }

        }
              @media(max-width:350px) {

        .thankYouMsg {
            font-size:15px;
        }

        }
    </style>
</head>
<body>
      <div class="wrapper">
    <header>
        <div class="container">
            <div class="logo"><a href="/"><img src="/images/logo.png" alt="Lucky 7 Sweeps" /></a></div>
        </div>
    </header>
    <div class="menu_bg">
        <div class="container">
            <nav id="cssmenu">
                <div id="head-mobile"></div>
                <div class="button"></div>
                <ul>
                    <li class="active"><a href="/">HOME</a></li>
                    <li><a href="/Lifestyle">LIFESTYLES</a>
                        <ul>
                            <asp:Literal runat="server" ID="litLifestyle"></asp:Literal>
                        </ul>
                    </li>
                    <li><a href="/Content/120/Get_Coupons/">GET COUPONS</a></li>
                    <li><a href="/Contests">SWEEPS & CONTESTS</a></li>
                    <li><a href="/Content/121/Financial_Support/">FINANCIAL SUPPORT</a></li>
                </ul>
            </nav>
        </div>
    </div>
          <div class="thankYou_wrapper">
              <div class="thankYouMsg">Thank you for your feedback.<br class="mobileOnly" /> Check out what we offer while you’re here!</div>
              <div class="startHere_div"><a class="startHere btn_thankyou" href="/"><span>Start Here</span></a></div>
        </div>
        <div class="clear"></div>
                  <div class="push"></div>
        </div>
            <footer>
        <div class="container">
            <div class="copy">Lucky7Sweeps&#8482; Copyright &copy; <%=DateTime.Now.Year %>. &nbsp;&nbsp;<a href="/Privacy.aspx">Privacy Policy</a></div>
        </div>
    </footer>


</body>
</html>
