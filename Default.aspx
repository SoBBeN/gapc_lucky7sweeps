﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentBodyTop" Runat="Server">
    <div class="header_section">
        <div class="Money"><img src="images/girl.png" alt="Money" /></div>
        <div class="form_section">
            <div class="form_body">
                <form runat="server" id="form1">
                    <div class="title">Sign Up Today!</div>
                    <div class="body">Have you been looking for access to the web's most exclusive contests and giveaways? Then look no further! All you have to do is sign up below, and you'll be eligible to start winning CASH prizes, gifts, and other rewards as provided by Lucky 7 Sweeps. Enter your email below to get started:</div>
                    <div class="field"><asp:TextBox type="email" required  runat="server" ID="txtiliketodrinkatlunch"></asp:TextBox></div>
                    <div class="disclaimer"><asp:CheckBox runat="server" ID="chkPrivacy" Checked="false" /> By checking this box you agree that you are a US Resident over the age of 18 and, agree to the Privacy Policy.</div>
                    <div class="disclaimer"><asp:CheckBox runat="server" ID="chkDailyEmail" Checked="false" />  You understand that by submitting your email address that you will be receiving daily email under the Lucky 7 Sweeps brand.</div>
                    <div class="g-recaptcha" data-sitekey="6LfKnxEUAAAAAO1iXBX9FqL0w-68XqXGl3UPBF5p"></div>
                    <div class="submit"><asp:ImageButton runat="server" ID="btnSubmit" OnClick="btnSubmit_Click"  /></div>
                </form>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentBody" Runat="Server">
<%--    <div class="clear"></div>
    <h1><a href="/Lifestyle"><img src="images/lifestyle.png" alt="Lifestyles" /><asp:Literal runat="server" ID="litLifestylesTitle">LIFESTYLES</asp:Literal></a></h1>
    <hr class="blueline" />
    <div class="home_section">
        <asp:Literal runat="server" ID="litArticles"></asp:Literal>
    </div>--%>
    <br class="clear" />
    <h1><a href="/Content/120/Get_Coupons/"><img src="images/top_secret.png" alt="Coupons" /><asp:Literal runat="server" ID="Literal3">COUPONS</asp:Literal></a></h1>
    <hr class="blueline" />
    <div class="home_section">
        <asp:Literal runat="server" ID="litCoupon"></asp:Literal>
    </div>
    <br class="clear" />
    <h1><a href="/Contests"><img src="images/contest_cup.png" alt="Sweeps & Contests" /><asp:Literal runat="server" ID="Literal1">SWEEPS & CONTESTS</asp:Literal></a></h1>
    <hr class="blueline" />
    <div class="home_section">
        <asp:Literal runat="server" ID="litSweeps"></asp:Literal>
    </div>
    <br class="clear" />
    <h1><a href="/Content/121/Financial_Support/"><img src="images/financial_support.png" alt="Financial Support" /><asp:Literal runat="server" ID="Literal5">FINANCIAL SUPPORT</asp:Literal></a></h1>
    <hr class="blueline" />
    <div class="home_section">
        <asp:Literal runat="server" ID="litFiancial"></asp:Literal>
    </div>
    <br class="clear" />
</asp:Content>