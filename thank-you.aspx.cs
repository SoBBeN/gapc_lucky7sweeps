﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class thank_you : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		FillMenuWithLifestyle();
	}
	private void FillMenuWithLifestyle()
	{
		StringBuilder sb = new StringBuilder();

		SqlDataReader dr = Functions.GetAllLifestyleCategories();
		if (dr != null && dr.HasRows)
		{
			while (dr.Read())
			{
				sb.Append("<li><a href=\"/Lifestyle/");
				sb.Append(dr["ID"]);
				sb.Append("/");
				sb.Append(Functions.StrToURL(Convert.ToString(dr["Description"])));
				sb.Append("/");
				sb.Append("\">");
				sb.Append(Convert.ToString(dr["Description"]));
				sb.Append("</a></li>");
			}
			dr.Close();
			dr.Dispose();
		}
		litLifestyle.Text = sb.ToString();
	}
}